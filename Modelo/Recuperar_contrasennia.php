<?php

namespace MVC\Modelo;

class Recuperar_contrasennia extends Conexion
{
    public $id;
    public $ID_codigos; 
    public $correo;
    public $codigo;
    public $fecha_expiracion;
    public $recuperado;

    public function insertar()
  {
    $pre=mysqli_prepare($this->con,"INSERT INTO recuperar_contraseña(ID_codigos,correo,codigo,fecha_expiracion,recuperado) VALUES (?,?,?,?,?,?)");

    $pre->bind_param("isssi",$this->ID_codigos,$this->correo,$this->codigo,$this->fecha_expiracion,$this->recuperado);

    $pre->execute();
    
    

    $pre=mysqli_prepare($this->con,"SELECT LAST_INSERT_ID() id");

    $pre->execute();

    $r=$pre->get_result();

    $this->ID_vendedor=$r->fetch_assoc()["id"];
    
    return true;
  }

  static function find($ID_usuarios)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"SELECT * FROM recuperar_contraseña WHERE  id=?");
			$pre->bind_param ("i", $id);
			$pre->execute();
			$res=$pre->get_result();
			return $res->fetch_object(Recuperar_contrasennia::class);

		}
		static function findbytype($codigo)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"SELECT * FROM recuperar_contraseña WHERE codigo=?");
			$pre->bind_param ("i", $codigo);
			$pre->execute();
			$res=$pre->get_result();
			return $res->fetch_object(Recuperar_contrasennia::class);
		}
		static function selectall ()
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"SELECT * FROM recuperar_contraseña");
			$pre->execute();
			$res=$pre->get_result();
			return $res->fetch_object(Recuperar_contrasennia::class);
        }
        
        public function editar()
		{
			$me =new Conexion();
			$pre=mysqli_prepare($me->con,"UPDATE recuperar_contraseña SET ID_productos=?,correo=?,codigo=?,fecha_expiracion=?, WHERE id=?");
			$pre->bind_param("isisi",$this->ID_Productos,$this->correo,$this->codigo,$this->fecha_expiracion,$this->id);
			$pre->execute();
			$res=$pre->get_result();
			return true;
		}

		static function eliminar($id)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"DELETE FROM recuperar_contraseña WHERE id=?");
			$pre->bind_param ("i", $id);
			$pre->execute();
			$res=$pre->get_result();
			return true;
		}



}


?>
<?php

namespace MVC\modelo;

class Vendedor extends Conexion

{
  public $ID_vendedor;
  public $NomVendedor;
  public $TipoVendedor;

  		public function access($NomVendedor)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"SELECT * FROM vendedor WHERE NomVendedor=?");
			$pre->bind_param("s",$NomVendedor);
			$pre->execute();
			$res=$pre->get_result();
			return $res->fetch_assoc();
		}

  public function insertar()
  {
    $pre=mysqli_prepare($this->con,"INSERT INTO vendedor(NomVendedor,TipoVendedor) VALUES (?,?)");

    $pre->bind_param("ss",$this->NomVendedor,$this->TipoVendedor);

    $pre->execute();
    echo json_encode($pre);
    

    $pre_=mysqli_prepare($this->con,"SELECT LAST_INSERT_ID() ID_vendedor");

    $pre_->execute();

    $r=$pre_->get_result();

    $this->ID_vendedor=$r->fetch_assoc()["ID_vendedor"];
    
    return true;
  }

  static function find($ID_vendedor)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"SELECT * FROM vendedor WHERE  ID_vendedor=?");
			$pre->bind_param ("i", $ID_vendedor);
			$pre->execute();
			$res=$pre->get_result();
			return $res->fetch_object(Vendedor::class);

		}
		static function findbytype($TipoVendedor)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"SELECT * FROM vendedor WHERE TipoVendedor=?");
			$pre->bind_param ("i", $TipoVendedor);
			$pre->execute();
			$res=$pre->get_result();
			return $res->fetch_object(Vendedor::class);
		}
		static function selectall ()
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"SELECT * FROM vendedor");
			$pre->execute();
			$res=$pre->get_result();
			return $res->fetch_object(Vendedor::class);
        }
        
        public function editar()
		{
			$me =new Conexion();
			$pre=mysqli_prepare($me->con,"UPDATE vendedor SET NomVendedor=?,TipoVendedor=? WHERE ID_vendedor=?");
			$pre->bind_param("ssi",$this->NomVendedor,$this->TipoVendedor,$this->ID_vendedor);
			$pre->execute();
			$res=$pre->get_result();
			return true;
		}

		static function eliminar($ID_vendedor)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"DELETE FROM vendedor WHERE ID_vendedor=?");
			$pre->bind_param ("i", $ID_vendedor);
			$pre->execute();
			$res=$pre->get_result();
			return true;
		}


}


?>
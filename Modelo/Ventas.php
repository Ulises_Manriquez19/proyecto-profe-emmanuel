<?php

namespace MVC\Modelo;

class Ventas extends Conexion
{
    public $ID_usuarios;
    public $ID_productos;
    public $ID_vendedor;
    public $ID_provedores;


    public function insertar()
    {

        $pre=mysqli_prepare($this->con,"INSERT INTO ventas(ID_usuarios,ID_productos,ID_vendedor,ID_provedores) VALUES (?,?,?,?)");
        $pre->bind_param("iiii",$this->ID_usuarios,$this->ID_productos,$this->ID_vendedor,$this->ID_provedores);
        $pre->execute();
        echo json_encode($pre);
        $pre_=mysqli_prepare($this->con,"SELECT LAST_INSERT_ID() id");
        $pre_->execute();
        $r=$pre_->get_result();
        $this->ID_provedores=$r->fetch_assoc()["id"];
        return true;

    }
    static function find($ID_usuarios)
    {
        $me=new Conexion();
        $pre=mysqli_prepare($me->con,"SELECT * FROM ventas WHERE ID_usuarios=?");
        $pre->bind_param ("i", $ID_usuarios);
        $pre->execute();
        $res=$pre->get_result();
        return $res->fetch_object(Ventas::class);

    }
    static function findbyproduct($ID_provedores)
    {
        $me=new Conexion();
        $pre=mysqli_prepare($me->con,"SELECT * FROM ventas WHERE ID_provedores=?");
        $pre->bind_param ("i", $ID_provedores);
        $pre->execute();
        $res=$pre->get_result();
        return $res->fetch_object(Ventas::class);
    }
    static function selectall ()
    {
        $me=new Conexion();
        $pre=mysqli_prepare($me->con,"SELECT * FROM ventas");
        $pre->execute();
        $res=$pre->get_result();
        return $res->fetch_object(Provedores::class);
    }

    function editar()
    {
        $pre=mysqli_prepare($this->con,"UPDATE ventas SET ID_usuarios=?,ID_productos=?,ID_vendedor=?, ID_provedores=? WHERE ID_usuarios=?");
        $pre->bind_param("iiiii",$this->ID_usuarios,$this->ID_productos,$this->ID_vendedor,$this->ID_provedores,$this->ID_usuarios);
        $pre->execute();
    }

    static function eliminar($ID_usuarios)
    {
        $me=new Conexion();
        $pre=mysqli_prepare($me->con,"DELETE FROM ventas WHERE ID_usuarios=?");
        $pre->bind_param("i",$ID_usuarios);
        $pre->execute();
        $res=$pre->get_result();
        return true;
    }
}
?>
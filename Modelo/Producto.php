<?php
  
  namespace MVC\modelo;

  class Producto extends Conexion

{
    public $ID_productos;
    public $TipoProducto;
    public $Precio;
    public $Unidades;
    public $Marca;

    public function insertar()
  {
    $pre=mysqli_prepare($this->con,"INSERT INTO productos(TipoProducto,Precio,Unidades,Marca) VALUES (?,?,?,?)");

    $pre->bind_param("siis",$this->TipoProducto,$this->Precio,$this->Unidades,$this->Marca);

    $pre->execute();
    echo json_encode($pre);
    

    $pre_=mysqli_prepare($this->con,"SELECT LAST_INSERT_ID() ID_productos");

    $pre_->execute();

    $r=$pre_->get_result();

    $this->ID_vendedor=$r->fetch_assoc()["ID_productos"];
    
    return true;
  }

  static function find($ID_productos)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"SELECT * FROM productos WHERE  ID_productos=?");
			$pre->bind_param ("i", $ID_productos);
			$pre->execute();
			$res=$pre->get_result();
			return $res->fetch_object(Producto::class);

		}
		static function findbytype($TipoProducto)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"SELECT * FROM productos WHERE TipoProducto=?");
			$pre->bind_param ("i", $TipoProducto);
			$pre->execute();
			$res=$pre->get_result();
			return $res->fetch_object(Producto::class);
		}
		static function selectall ()
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"SELECT * FROM productos");
			$pre->execute();
			$res=$pre->get_result();
			return $res->fetch_object(Producto::class);
        }
        
        public function editar()
		{
			$me =new Conexion();
			$pre=mysqli_prepare($me->con,"UPDATE productos SET TipoProducto=?,Precio=?,Unidades=?,Marca=? WHERE ID_productos=?");
			$pre->bind_param("siisi",$this->TipoProducto,$this->Precio,$this->Unidades,$this->Marca,$this->ID_productos);
			$pre->execute();
			$res=$pre->get_result();
			return true;
		}

		static function eliminar($ID_productos)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"DELETE FROM productos WHERE ID_productos=?");
			$pre->bind_param ("i", $ID_productos);
			$pre->execute();
			$res=$pre->get_result();
			return true;
		}


}  


?>
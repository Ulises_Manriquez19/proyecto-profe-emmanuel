<?php 

	namespace MVC\modelo;
	
	class Usuario extends Conexion
 {	
		public $id;
		public $NomUsuario;
		public $Correo;
		public $ContraUsuario;

		public function insertar()
		{
			
			$pre=mysqli_prepare($this->con,"INSERT INTO usuarios(NomUsuario,Correo,ContraUsuario) VALUES (?,?,?)");

			$pre->bind_param("sss",$this->NomUsuario,$this->Correo,$this->ContraUsuario);

			$pre->execute();
			echo json_encode($pre);
			

			$pre_=mysqli_prepare($this->con,"SELECT LAST_INSERT_ID() id");

			$pre_->execute();

			$r=$pre_->get_result();

			$this->ID_usuario=$r->fetch_assoc()["id"];
			

			return true;
			
		}

		static function access($NomUsuario)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"SELECT * FROM usuarios WHERE NomUsuario=?");
			$pre->bind_param("s",$NomUsuario);
			$pre->execute();
			$res=$pre->get_result();
			return $res->fetch_assoc();
		}

		
		static function find($id)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"SELECT * FROM usuarios WHERE id=?");
			$pre->bind_param ("i", $id);
			$pre->execute();
			$res=$pre->get_result();
			return $res->fetch_object(Usuario::class);

		}
		static function findbyemail($Correo)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"SELECT * FROM usuarios WHERE Correo=?");
			$pre->bind_param ("i", $Correo);
			$pre->execute();
			$res=$pre->get_result();
			return $res->fetch_object(Usuario::class);
		}
		static function selectall ()
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"SELECT * FROM usuarios");
			$pre->execute();
			$res=$pre->get_result();
			return $res->fetch_object(Usuario::class);
		}

		public function editar()
		{
			$me =new Conexion();
			$pre=mysqli_prepare($me->con,"UPDATE usuarios SET NomUsuario=?,Correo=?,ContraUsuario=? WHERE id=?");
			$pre->bind_param("sssi",$this->NomUsuario,$this->Correo,$this->ContraUsuario,$this->id);
			$pre->execute();
			$res=$pre->get_result();
			return true;
		}

		static function eliminar($id)
		{
			$me=new Conexion();
			$pre=mysqli_prepare($me->con,"DELETE FROM usuarios WHERE id=?");
			$pre->bind_param ("i", $id);
			$pre->execute();
			$res=$pre->get_result();
			return true;
		}

		


}


 ?>
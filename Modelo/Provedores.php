<?php

namespace MVC\Modelo;

class Provedores extends Conexion
{
    public $ID_provedores;
    public $nombreprov;
    public $ID_producto;

    public function insertar()
    {

        $pre=mysqli_prepare($this->con,"INSERT INTO provedores(nombreprov,ID_producto) VALUES (?,?)");
        $pre->bind_param("si",$this->nombreprov,$this->ID_producto);
        $pre->execute();
        echo json_encode($pre);
        $pre_=mysqli_prepare($this->con,"SELECT LAST_INSERT_ID() ID_provedores");
        $pre_->execute();
        $r=$pre_->get_result();
        $this->ID_provedores=$r->fetch_assoc()["ID_provedores"];
        return true;

    }
    static function find($ID_provedores)
    {
        $me=new Conexion();
        $pre=mysqli_prepare($me->con,"SELECT * FROM provedores WHERE ID_provedores=?");
        $pre->bind_param ("i", $ID_provedores);
        $pre->execute();
        $res=$pre->get_result();
        return $res->fetch_object(Provedores::class);

    }
    static function findbyemail($nombreprov)
    {
        $me=new Conexion();
        $pre=mysqli_prepare($me->con,"SELECT * FROM provedores WHERE nombreprov=?");
        $pre->bind_param ("i", $nombreprov);
        $pre->execute();
        $res=$pre->get_result();
        return $res->fetch_object(Provedores::class);
    }
    static function selectall ()
    {
        $me=new Conexion();
        $pre=mysqli_prepare($me->con,"SELECT * FROM provedores");
        $pre->execute();
        $res=$pre->get_result();
        return $res->fetch_object(Provedores::class);
    }

    function editar()
    {
        $pre=mysqli_prepare($this->con,"UPDATE provedores SET ID_provedores=?,nombreprov=?,ID_producto=? WHERE ID_provedores=?");
        $pre->bind_param("isii",$this->ID_provedores,$this->nombreprov,$this->ID_producto,$this->ID_provedores);
        $pre->execute();
    }

    static function eliminar($ID_provedores)
    {
        $me=new Conexion();
        $pre=mysqli_prepare($me->con,"DELETE FROM provedores WHERE ID_Provedores=?");
        $pre->bind_param("i", $ID_provedores);
        $pre->execute();
        $res=$pre->get_result();
        return true;
    }
}
?>
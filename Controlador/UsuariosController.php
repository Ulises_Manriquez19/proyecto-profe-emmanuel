<?php 
	use MVC\Modelo\Usuario;
	include "Modelo/Conexion.php";
	include "Modelo/Usuario.php";
	class UsuariosController
	{ 
		function __construct()
		{
			 
		}
		
		function acceder()
		{
			$NomUsuario=$_POST["NomUsuario"];
			$Correo=$_POST["Correo"];
			$ContraUsuario=$_POST["ContraUsuario"];
			$usuario=Usuario::access($NomUsuario);
			if($usuario['NomUsuario']==$NomUsuario && $usuario['Correo']==$Correo && $usuario['ContraUsuario']==$ContraUsuario)
			{
				session_start();
				$user=$_SESSION['usuario']=$NomUsuario;
				echo json_encode($usuario);
			}
			else
			{
				echo json_encode(["estado"=>false]);
			}
		}
		
			public function create()
		{
			
			if(isset($_POST["NomUsuario"])&& isset($_POST["ContraUsuario"])&& isset($_POST["Correo"]))
			{
				$usuario=new Usuario();
				$usuario->NomUsuario= $_POST["NomUsuario"];
				$usuario->Correo= $_POST["Correo"];
				$usuario->ContraUsuario= $_POST["ContraUsuario"];
				$usuario->insertar();
				echo json_encode(["estatus"=>true,"usuario"=>$usuario]);
				
			}
			else
			{
				echo json_encode(["estatus"=>false,"message"=>"error"]);
			}
		}

		public function find()
		{	
			$id=$_POST["id"];
			$usuario=new Usuario();
			$usuario=$usuario::find($id);
			if ($id==true)
			{
				echo json_encode(["estatus"=>"true","usuario"=>$usuario]);	
			}
			else
			{
				echo json_encode(["estatus"=>"false","message"=>"error"]);
			}
			
			
		}
		
		public function select()
		{	
			
			
			$usuario=Usuario::selectall();
			echo json_encode(["estatus"=>"success","usuario"=>$usuario]);
		}

		public function findemail()
		{
			if(isset($_POST["Correo"]))
			{
				$Correo=$_POST["Correo"];
				$usuario=new Usuario();
				$usuario=$usuario->findbyemail($Correo);
				echo json_encode(["estatus"=>"true","usuario"=>$usuario]);	
			}
			else
			{
				echo json_encode(["estatus"=>"false","message"=>"error"]);
			}	
		}
		
		public function edit()
		{
			if(isset($_POST["NomUsuario"])&& isset($_POST["ContraUsuario"])&& isset($_POST["Correo"])&& isset($_POST["id"]))
			{
				$usuario=new Usuario();
				$usuario->NomUsuario= $_POST["NomUsuario"];
				$usuario->Correo= $_POST["Correo"];
				$usuario->ContraUsuario= $_POST["ContraUsuario"];
				$usuario->id=$_POST["id"];
				$usuario->editar();
				echo json_encode(["estatus"=>"true","usuario"=>$usuario]);	
			}else
			{
				echo json_encode(["estatus"=>"false","message"=>"error"]);
			}
		}

		public function delete()
		{
			if (isset($_POST["id"])) 
			{
				$id=$_POST["id"];
				$usuario=new Usuario();
				$usuario=$usuario->eliminar($id);
				echo json_encode(["estatus"=>"true","usuario"=>$usuario]);	
			}else{
				echo json_encode(["estatus"=>"false","message"=>"error"]);
			}
		}	
					

		
				
				
	}

 ?>
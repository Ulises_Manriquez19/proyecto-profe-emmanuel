<?php
    use MVC\Modelo\Vendedor;
	include "Modelo/Conexion.php";
    include "Modelo/Vendedor.php";
    class VendedorController
    {

		
        function _construct()
        {

        }

		function acceder()
		{
			$NomVendedor=$_POST["NomVendedor"];
			$TipoVendedor=$_POST["TipoVendedor"];
			$usuario=Vendedor::access($NomVendedor);
			if($usuario['NomVendedor']==$NomVendedor&&$usuario['TipoVendedor']==$TipoVendedor)
			{
				session_start();
				$user=$_SESSION['usuario']=$NomVendedor;
				echo json_encode($usuario);
			}
			else
			{
				echo json_encode(["estado"=>false]);
			}
		}

        public function create()
        {
			if(isset($_POST["NomVendedor"])&& isset($_POST["TipoVendedor"]))
			{
			$vendedor=new Vendedor();
            $vendedor->NomVendedor=$_POST["NomVendedor"];
            $vendedor->TipoVendedor=$_POST["TipoVendedor"];
			$vendedor->insertar();
				echo json_encode(["estatus"=>"true","usuario"=>$vendedor]);
			}else{
				echo json_encode(["estatus"=>"false","message"=>"error"]);
			}
            
        }

        public function find()
		{	
			$id=$_POST["ID_vendedor"];
			$vendedor=new Vendedor();
			$vendedor=$vendedor::find($id);
			if ($id==true) 
			{
				echo json_encode(["estatus"=>"true","usuario"=>$vendedor]);	
			}else{
				echo json_encode(["estatus"=>"false","message"=>"error"]);
			}
		}

		public function findtipo()
		{
			if (isset($_POST["TipoVendedor"])) 
			{
				$TipoVendedor=$_POST["TipoVendedor"];
				$vendedor=new Vendedor();
				$vendedor=$vendedor->findbytype($TipoVendedor);
				echo json_encode(["estatus"=>"true","usuario"=>$vendedor]);								
			}else{
				echo json_encode(["estatus"=>"false","message"=>"error"]);
			}
			
		}
		
		public function edit()
		{
			if(isset($_POST["NomVendedor"])&& isset($_POST["TipoVendedor"])&& isset($_POST["ID_vendedor"]))
			{
				$vendedor=new Vendedor();
				$vendedor->NomVendedor= $_POST["NomVendedor"];
				$vendedor->TipoVendedor= $_POST["TipoVendedor"];
				$vendedor->ID_vendedor=$_POST["ID_vendedor"];
				$vendedor->editar();
				echo json_encode(["estatus"=>"true","usuario"=>$vendedor]);								
			}else
			{
				echo json_encode(["estatus"=>"false","message"=>"error"]);
			}
			

		}

		public function delete()
		{
			if (isset($_POST["ID_vendedor"]))
			{
				$id=$_POST["ID_vendedor"];
				$vendedor=new Vendedor();
				$vendedor=$vendedor->eliminar($id);
				echo json_encode(["estatus"=>"true","usuario"=>$vendedor]);								
			}else
			{
				echo json_encode(["estatus"=>"false","message"=>"error"]);
			}
			
		}
    }


?>
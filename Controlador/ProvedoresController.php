<?php

use MVC\Modelo\Provedores;
include "Modelo/Conexion.php";
include "Modelo/Provedores.php";

class ProvedoresController
{


    function __construct()
    {

    }

    public function create()
    {
        if (isset($_POST["nombreprov"])&& isset($_POST["ID_producto"])) 
        {
            $provedores=new Provedores();
            $provedores->nombreprov= $_POST["nombreprov"];
            $provedores->ID_producto= $_POST["ID_producto"];
            $provedores->insertar();
            echo json_encode(["estatus"=>"success","usuario"=>$provedores]);
        }else{
            echo json_encode(["estatus"=>"false","message"=>"error"]);
        }    
    }

    public function find()
    {
        if (isset($_POST["ID_provedores"])) 
        {
            $ID=$_POST["ID_provedores"];
            $provedores=new Provedores();
            $provedores=$provedores::find($ID);
            echo json_encode(["estatus"=>"success","usuario"=>$provedores]);
        }else{
            echo json_encode(["estatus"=>"false","message"=>"error"]);
        }   

    }

    public function findbyemail()
    {
        if (isset($_POST["nombreprov"]))
        {
            $nombreprov=$_POST["nombreprov"];
            $provedores=new Provedores();
            $provedores=$provedores->findbyemail($nombreprov);
            echo json_encode(["estatus"=>"success","usuario"=>$provedores]);
        }else{
            echo json_encode(["estatus"=>"false","message"=>"error"]);
        }   
         
    }

    public function update()
    {
        if (isset($_POST["ID_provedores"])&& isset($_POST["nombreprov"])&& isset($_POST["ID_producto"]))
        {
            $provedores =new Provedores();
            $provedores->ID_provedores=$_POST["ID_provedores"];
            $provedores->nombreprov=$_POST["nombreprov"];
            $provedores->ID_producto=$_POST["ID_producto"];
            $provedores->editar();
            echo json_encode(["estatus"=>"success","usuario"=>$provedores]);
        }else{
            echo json_encode(["estatus"=>"false","message"=>"error"]);
        }   
        
    }

    public function delete()
    {
        if (isset($_POST["ID_provedores"])) 
        {
            $id=$_POST["ID_provedores"];
            $provedores= new Provedores();
            $provedores=$provedores->eliminar($id);
            echo json_encode(["estatus"=>"success","usuario"=>$provedores]);
        }else{
            echo json_encode(["estatus"=>"false","message"=>"error"]);
        }   
    }
}
?>
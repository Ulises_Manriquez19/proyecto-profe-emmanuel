<?php
    use MVC\Modelo\Producto;
	include "Modelo/Conexion.php";
    include "Modelo/Producto.php";
    class ProductoController
    {

		
        function _construct()
        {

        }

        public function create()
        {
			if(isset($_POST["TipoProducto"])&& isset($_POST["Precio"])&& isset($_POST["Unidades"])&& isset($_POST["Marca"]))
			{
				$producto=new Producto();
            	$producto->TipoProducto=$_POST["TipoProducto"];
            	$producto->Precio=$_POST["Precio"];
            	$producto->Unidades=$_POST["Unidades"];
            	$producto->Marca=$_POST["Marca"];
            	$producto->insertar();
				echo json_encode(["estatus"=>"success","usuario"=>$producto]);   
			}else
			{
				echo json_encode(["estatus"=>"false","message"=>"error"]);
			}           
        }

        public function find()
		{	
			if (isset($_POST["ID_productos"])) 
			{
				$id=$_POST["ID_productos"];
				$producto=new Producto();
				$producto=$producto::find($id);
				echo json_encode(["estatus"=>"success","usuario"=>$producto]);   
			}else
			{
				echo json_encode(["estatus"=>"false","message"=>"error"]);
			}   	
			
			
		}

		public function findbytype()
		{
			if (isset($_POST["TipoProducto"]))
			{
				$TipoProducto=$_POST["TipoProducto"];
				$producto=new Producto();
				$producto=$producto->findbytype($TipoProducto);
				echo json_encode(["estatus"=>"success","usuario"=>$producto]);   
			}else
			{
				echo json_encode(["estatus"=>"false","message"=>"error"]);
			}   
			
		}
		
		public function edit()
		{
			if(isset($_POST["TipoProducto"])&& isset($_POST["Precio"])&& isset($_POST["Unidades"])&& isset($_POST["Marca"])&& isset($_POST["ID_productos"]))
			{
				$producto=new Producto();
            	$producto->TipoProducto=$_POST["TipoProducto"];
            	$producto->Precio=$_POST["Precio"];
            	$producto->Unidades=$_POST["Unidades"];
            	$producto->Marca=$_POST["Marca"];
				$producto->ID_productos=$_POST["ID_productos"];
				$producto->editar();
				echo json_encode(["estatus"=>"success","usuario"=>$producto]);   
			}else
			{
				echo json_encode(["estatus"=>"false","message"=>"error"]);
			}   
		}

		public function delete()
		{
			if (isset($_POST["ID_productos"]))
			{
				$id=$_POST["ID_productos"];
				$producto=new Producto();
				$producto=$producto->eliminar($id);
				echo json_encode(["estatus"=>"success","usuario"=>$producto]);   
			}else
			{
				echo json_encode(["estatus"=>"false","message"=>"error"]);
			}   			
		}
    }


?>
<?php

use MVC\Modelo\Ventas;
include "Modelo/Conexion.php";
include "Modelo/Ventas.php";

class VentasController
{


    function __construct()
    {

    }

    public function create()
    {
        if(isset($_POST["ID_usuarios"])&& isset($_POST["ID_productos"])&& isset($_POST["ID_vendedor"])&& isset($_POST["ID_provedores"]))
        {
            $ventas=new Ventas();
            $ventas->ID_usuarios= $_POST["ID_usuarios"];
            $ventas->ID_productos= $_POST["ID_productos"];
            $ventas->ID_vendedor= $_POST["ID_vendedor"];
            $ventas->ID_provedores= $_POST ["ID_provedores"];
            $ventas->insertar();
            echo json_encode(["estatus"=>"success","usuario"=>$ventas]);   
        }else{
            echo json_encode(["estatus"=>"false","message"=>"error"]);
        }
        
    }

    public function find()
    {
        if (isset($_POST["ID_usuarios"]))
        {
            $id=$_POST["ID_usuarios"];
            $ventas=new Ventas();
            $ventas=$ventas->find($id);   
            echo json_encode(["estatus"=>"success","usuario"=>$ventas]);   
        }else{
            echo json_encode(["estatus"=>"false","message"=>"error"]);
        }     
    }

    public function findbyproduct()
    {
        if (isset($_POST["ID_productos"]))
        {   
        $ID_productos=$_POST["ID_productos"];
        $ventas=new Ventas();
        $ventas=$ventas->findbyproduct($ID_productos);
        echo json_encode(["estatus"=>"success","usuario"=>$ventas]);   
        }else{
            echo json_encode(["estatus"=>"false","message"=>"error"]);
        }   
    }

    public function update()
    {
        if(isset($_POST["ID_usuarios"])&& isset($_POST["ID_productos"])&& isset($_POST["ID_vendedor"])&& isset($_POST["ID_provedores"]))
        {
            $ventas = new Ventas();
            $ventas->ID_usuarios=$_POST["ID_usuarios"];
            $ventas->ID_productos=$_POST["ID_productos"];
            $ventas->ID_vendedor=$_POST["ID_vendedor"];
            $ventas->ID_provedores=$_POST["ID_provedores"];
            $ventas->editar();
            echo json_encode(["estatus"=>"success","usuario"=>$ventas]);   
        }else{
            echo json_encode(["estatus"=>"false","message"=>"error"]);
        }
        
    }

    public function delete()
    {
        if (isset($_POST["ID_usuarios"])) 
        {
            $ID_usuarios=$_POST["ID_usuarios"];
            $ventas=new Ventas(); 
            $ventas=$ventas->eliminar($ID_usuarios);
            echo json_encode(["estatus"=>"success","usuario"=>$ventas]);   
        }else{
            echo json_encode(["estatus"=>"false","message"=>"error"]);
        }
        
    }
}

?>
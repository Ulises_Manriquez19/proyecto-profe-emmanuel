-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-03-2020 a las 23:47:54
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tienda_online`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `ID_productos` int(11) NOT NULL,
  `TipoProducto` varchar(30) DEFAULT NULL,
  `Precio` int(5) DEFAULT NULL,
  `Unidades` int(5) DEFAULT NULL,
  `Marca` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`ID_productos`, `TipoProducto`, `Precio`, `Unidades`, `Marca`) VALUES
(1, 'motherboard', 1500, 1, 'asus');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provedores`
--

CREATE TABLE `provedores` (
  `ID_provedores` int(11) NOT NULL,
  `nombreprov` varchar(30) DEFAULT NULL,
  `ID_producto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `provedores`
--

INSERT INTO `provedores` (`ID_provedores`, `nombreprov`, `ID_producto`) VALUES
(1, 'luis', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recuperar_contraseña`
--

CREATE TABLE `recuperar_contraseña` (
  `ID_codigos` int(6) NOT NULL,
  `id` int(11) NOT NULL,
  `correo` varchar(30) NOT NULL,
  `codigo` varchar(6) NOT NULL,
  `fecha_expiracion` date NOT NULL,
  `recuperado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `NomUsuario` varchar(30) DEFAULT NULL,
  `Correo` varchar(30) DEFAULT NULL,
  `ContraUsuario` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `NomUsuario`, `Correo`, `ContraUsuario`) VALUES
(8, 'ulises', 'ulises_romero@hotmail.com', 'ulise123'),
(11, 'akahd', NULL, 'qpepe123');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vendedor`
--

CREATE TABLE `vendedor` (
  `ID_vendedor` int(11) NOT NULL,
  `NomVendedor` varchar(30) DEFAULT NULL,
  `TipoVendedor` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vendedor`
--

INSERT INTO `vendedor` (`ID_vendedor`, `NomVendedor`, `TipoVendedor`) VALUES
(1, 'ricardo', 'gerente '),
(2, 'ulises', 'gerente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `ID_usuarios` int(11) DEFAULT NULL,
  `ID_productos` int(11) DEFAULT NULL,
  `ID_vendedor` int(11) DEFAULT NULL,
  `ID_provedores` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`ID_usuarios`, `ID_productos`, `ID_vendedor`, `ID_provedores`) VALUES
(8, 1, 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`ID_productos`);

--
-- Indices de la tabla `provedores`
--
ALTER TABLE `provedores`
  ADD PRIMARY KEY (`ID_provedores`),
  ADD KEY `ID_producto` (`ID_producto`);

--
-- Indices de la tabla `recuperar_contraseña`
--
ALTER TABLE `recuperar_contraseña`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `vendedor`
--
ALTER TABLE `vendedor`
  ADD PRIMARY KEY (`ID_vendedor`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD KEY `ID_usuarios` (`ID_usuarios`),
  ADD KEY `ID_productos` (`ID_productos`),
  ADD KEY `ID_vendedor` (`ID_vendedor`),
  ADD KEY `ID_provedores` (`ID_provedores`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `ID_productos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `provedores`
--
ALTER TABLE `provedores`
  MODIFY `ID_provedores` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `vendedor`
--
ALTER TABLE `vendedor`
  MODIFY `ID_vendedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `provedores`
--
ALTER TABLE `provedores`
  ADD CONSTRAINT `provedores_ibfk_1` FOREIGN KEY (`ID_producto`) REFERENCES `productos` (`ID_productos`);

--
-- Filtros para la tabla `recuperar_contraseña`
--
ALTER TABLE `recuperar_contraseña`
  ADD CONSTRAINT `recuperar_contraseña_ibfk_1` FOREIGN KEY (`id`) REFERENCES `usuarios` (`id`);

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `ventas_ibfk_1` FOREIGN KEY (`ID_usuarios`) REFERENCES `usuarios` (`id`),
  ADD CONSTRAINT `ventas_ibfk_2` FOREIGN KEY (`ID_productos`) REFERENCES `productos` (`ID_productos`),
  ADD CONSTRAINT `ventas_ibfk_3` FOREIGN KEY (`ID_vendedor`) REFERENCES `vendedor` (`ID_vendedor`),
  ADD CONSTRAINT `ventas_ibfk_4` FOREIGN KEY (`ID_provedores`) REFERENCES `provedores` (`ID_provedores`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

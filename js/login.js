function enviar() {
    var e= $("#email").val();
    var p = $("#pass").val();
    var n= $("#nombre").val();
    if (e!=""&& p != "" && n != "") {
        $.ajax({
            url: 'action/registrar.php',
            type: 'POST',
            data: {email:e,pass:p,nombre:n},
            dataType: 'json',
            success: function(response) {
                if (response.success == true) {
                     $("#submitReg")[0].reset();
                     $('#add').html('<h3 id="msj" style="color:green">' + response.messages +
                        '</h3>');
                    $("#msj").delay(500).show(10, function() {
                        $(this).delay(3000).hide(10, function() {
                            $(this).remove();
                        });
                    });
                }else{
                    $('#add').html('<h3 id="msj" style="color:red"> ' + response.messages +
                        '</h3>');
                    $("#msj").delay(500).show(10, function() {
                        $(this).delay(3000).hide(10, function() {
                            $(this).remove();
                        });
                    });
                }
            }
        });
    }
}